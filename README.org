* UI DESIGN FOR INFIX TO POSTFIX EXPERIMENT
- Here are the UI artifacts for our experiment on infix to postfix conversion and evaluation and the images we used  on our VLEAD project



* Repository Structure 
  #+BEGIN_EXAMPLE
    ui-design/
    |--- README.org
    |--- init.sh
    |--- makefile
    |--- src/
         |--- index.org
         |--- img/
         |--- ui-artefacts/
  #+END_EXAMPLE
